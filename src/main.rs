use std::io;
use std::io::Write;
use std::vec::Vec;


fn to_morse() {
  print!("Input a sentence to translate to morse code: ");
  io::stdout().flush().ok().expect("Could not flush stdout");

  let mut sentence = String::new();
  io::stdin().read_line(&mut sentence)
    .expect("Invalid input");
  sentence.pop();

  for c in sentence.to_ascii_lowercase().chars() {
    match c {
      'a' => print!(".- "),
      'b' => print!("-... "),
      'c' => print!("-.-. "),
      'd' => print!("-.. "),
      'e' => print!(". "),
      'f' => print!("..-. "),
      'g' => print!("--. "),
      'h' => print!(".... "),
      'i' => print!(".. "),
      'j' => print!(".--- "),
      'k' => print!("-.- "),
      'l' => print!(".-.. "),
      'm' => print!("-- "),
      'n' => print!("-. "),
      'o' => print!("--- "),
      'p' => print!(".--. "),
      'q' => print!("--.- "),
      'r' => print!(".-. "),
      's' => print!("... "),
      't' => print!("- "),
      'u' => print!("..- "),
      'v' => print!("...- "),
      'w' => print!(".-- "),
      'x' => print!("-..- "),
      'y' => print!("-.-- "),
      'z' => print!("--.. "),
      ' ' => print!("/ "),
      _ => println!("\n You input a invalid character!")
    }
    io::stdout().flush().ok().expect("Could not flush stdout");
 
  }
  println!("");
}



fn from_morse() {
  print!("Enter morse code you want to translate to text: ");
  io::stdout().flush().ok().expect("Could not flush stdout");

  let mut morse_code = String::new();
  io::stdin().read_line(&mut morse_code)
    .expect("invalid input");

  let mut word: Vec<char> = Vec::new();

  for c in morse_code.chars() {

    if c != ' ' && c != '\n' {
      word.push(c);

    } else {
      if word.is_empty() {
        println!("you didnt input anything")
      } else {
        let word_string = word.iter().cloned().collect::<String>();
        match word_string.as_ref() {
          ".-"    =>   print!("a"),
          "-..."  =>   print!("b"),
          "-.-."  =>   print!("c"),
          "-.."   =>   print!("d"),
          "."     =>   print!("e"),
          "..-."  =>   print!("f"),
          "--."   =>   print!("g"),
          "...."  =>   print!("h"),
          ".."    =>   print!("i"),
          ".---"  =>   print!("j"),
          "-.-"   =>   print!("k"),
          ".-.."  =>   print!("l"),
          "--"    =>   print!("m"),
          "-."    =>   print!("n"),
          "---"   =>   print!("o"),
          ".--."  =>   print!("p"),
          "--.-"  =>   print!("q"),
          ".-."   =>   print!("r"),
          "..."   =>   print!("s"),
          "-"     =>   print!("t"),
          "..-"   =>   print!("u"),
          "...-"  =>   print!("v"),
          ".--"   =>   print!("w"),
          "-..-"  =>   print!("x"),
          "-.--"  =>   print!("y"),
          "--.."  =>   print!("z"),
          "/"     =>   print!(" "),
           _      =>   println!("\n You input a invalid character!")
        }
        io::stdout().flush().ok().expect("Could not flush stdout");
        word.clear();
      }

    }
  }
  println!("");
}

fn main() {
  print!("1: translate to morse code \n2: Translate from morse code \nYour choice: ");
  io::stdout().flush().ok().expect("Could not flush stdout");
  let mut to_or_from = String::new();

  io::stdin().read_line(&mut to_or_from)
    .expect("invalid input");
  to_or_from.pop();

  match to_or_from.as_ref() {
    "1" => to_morse(),
    "2" => from_morse(),
    _ => println!("Invalid input")
  }
}